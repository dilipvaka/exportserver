
var server = require('http');
var Pool = require('./node_modules/phantomjs-pool/lib/phantomjs-pool').Pool;
var PAGES = [
     ];
var responseMap = {};
var jobId = 0;
// var base64 = require('base-64');


var service = server.createServer(function (request, response) {

    function parseGET(url){
      // adapted from http://stackoverflow.com/a/8486188
      var query = url.substr(url.indexOf("?")+1);
      var result = {};
      query.split("&").forEach(function(part) {
        var e = part.split("=");
        result[e[0]] = decodeURIComponent(e[1]);
      });
      return result;
    }

    var t = Date.now();
    console.log('Request received at ' + new Date());    
    var params = parseGET(request.url),
        url=params.baseURL+"?Server="+params.serverName+"&Project="+params.projectName+"&Port="+(params.serverPort||0)+"&evt=3140&src=mstrWeb.3140",
        messageID=params.messageID,
        documentID=params.documentID,
        sessionState = params.sessionState,
        JSESSIONID = params.JSESSIONID;

    
   
    var docSource = messageID?"&messageID="+messageID:"&documentID="+documentID;
    if(!docSource){
        response.write('error : include either the documentID or the messageID');
        response.close();
    }

    if(!/undefined/.test(url)){         // checking for 'undefined' string in url to avoid inserting in queue, like favicon requests
    
    jobId++ ;
    console.log("jobId of the request "+ jobId);
    PAGES.push({
        'jobId': jobId,
        'url': url,
        'sessionState': sessionState,
        'JSESSIONID': JSESSIONID,
        'docSource': docSource
        
    });    
    
   
    var respondToClient = (function(response,jobId,t){
        return function(some){
            
            response.on('error', function(err) {
            // This prints the error message and stack trace to `stderr`.
            console.error(err.stack);
            });
            

            console.log('writing response for job ' + jobId);
            response.write('<html><body>');
           
            var length = Object.keys(some[jobId]).length;            
            for(var i = 1; i <= length; i++)
            {
                response.write('<img alt="Embedded Image" src="data:image/png;base64,'+some[jobId][i]+'" />');

            }
            

            response.write('</body></html>');
            response.end();
            t = Date.now() - t;
            console.log("Loading time " + t/1000 + ' sec');
            
        }
    })(response,jobId,t);
    
    
    responseMap[jobId] = {callback:respondToClient};

    }else{
        
    }
    
 
});
service.setTimeout(600000, function(response){console.error('timeout occurred');response.write('timeout');response.end()});
service.listen(7088);


function jobCallback(job, worker, index) {

    // as long as we have urls that we want to crawl we execute the job
    var url = PAGES[index];
    
    if(url){    
        // the first argument contains the data which is passed to the worker
        // the second argument is a callback which is called when the job is executed
       job({
            url: url.url,
            id: index,
            sessionState: url.sessionState,
            JSESSIONID : url.JSESSIONID,
            docSource: url.docSource,
           
        }, function(err,workerData) {
            // Lets log if it worked
            if (err) {
                console.log('There were some problems for url ' + url + ': ' + err.message);
            } else {
                console.log('DONE NOW: ' + url + '(' + index + ')');
                
                responseMap[index+1].callback(workerData);
                           
            }

            
        });
       
    } else {
        // wait for sometime before checking the queue       
         setTimeout(function(){
            
              jobCallback(job, worker, index)           
            }, 1000);
    }


}


     var pool = new Pool({
    numWorkers : 3,
    jobCallback : jobCallback,
    verbose : true,
    spawnWorkerDelay : 100,
    phantomjsBinary : './phantomjs',
    phantomjsOptions : ["--disk-cache=true","--max-disk-cache-size=10000","--load-images=false"],
    workerFile : __dirname + '/worker.js' // location of our worker file (as an absolute path)
    });
    pool.start();

