
var webpage = require('webpage');
// worker needs to export one function which is called with the job
module.exports = function(data, done, worker) {

    // data contains the data we passed to the job function in the master file
    // done is a function which needs to be called to signal that the job is executed
    // worker contains some meta data about this worker (like the id)

    // we just fetch the page and save it as an base64image normally
    var page = webpage.create();
    phantom.addCookie({
      'name'     : 'JSESSIONID',   /* required property */
      'value'    : data.JSESSIONID,  /* required property */
      'path'     : '/',                /* required property */
      'httponly' : true,
      'secure'   : false,
      // 'domain'   : '192.168.8.246',
      'domain'   : 'localhost',
      'expires'  : (new Date()).getTime() + (1000 * 60 * 60)   /* <-- expires in 1 hour */
    });

    page.onNavigationRequested = function(url, type, willNavigate, main) {
        console.log('Trying to navigate to: ' + url);
        console.log('Caused by: ' + type);
        console.log('Will actually navigate: ' + willNavigate);
        console.log('Sent from the page\'s main frame: ' + main);
    }
    
    
    page.viewportSize = {
      width: 1280,
      height: 800
    };

    page.customHeaders = {
      "sessionState": data.sessionState
    };

    page.settings.headers = {"Content-Type":"application/x-www-form-urlencoded"}
    page.settings.userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36";

    var workerResult = {};
    var combo = {};



    page.open(data.url+data.docSource, function(status){

            page.onConsoleMessage = function(msg, lineNum, sourceId) {
                    console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
                };

            page.onLoadFinished = function(){
                

                var bb =  page.evaluate(function(){
                            return document.getElementsByClassName("mstrmojo-VIVizPanel-content")[0].getBoundingClientRect();
                    });

                page.clipRect = {
                        top:    bb.top,
                        left:   bb.left,
                        width:  bb.width,
                        height: bb.height
                    };
                    
                    setTimeout(function(){
                        
                        combo[1] = page.renderBase64('png');
	                    workerResult[data.id+1] = combo;
	                    done(null,workerResult); 
                        }, 5000);
                              
            };

               
            page.onError = function(error){
                console.log(error);
            };
        });
}

    
